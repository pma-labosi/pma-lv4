package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.ViewModelProvider

private const val TAG = "MainActivity";
class MainActivity : AppCompatActivity() {
    public lateinit var btnTrue: Button
    public lateinit var btnFalse: Button
    public lateinit var btnNext: Button
    public lateinit var btnBack: Button
    public lateinit var tvQuestion: TextView
    public var currentIndex: Int = 0
    private val questionList = listOf(
        Question(R.string.question_android, true),
        Question(R.string.question_calculator, false),
        Question(R.string.question_unity, true),
        Question(R.string.question_json, true)
    )
    private val quizViewModel: QuizViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "onCreate(Bundle?) called")
        setContentView(R.layout.activity_main)

        btnTrue = findViewById(R.id.btnTrue)
        btnFalse = findViewById(R.id.btnFalse)
        btnNext = findViewById(R.id.btnNext)
        btnBack = findViewById(R.id.btnBack)
        tvQuestion = findViewById(R.id.tvQuestion)

        tvQuestion.setText(questionList[currentIndex].textResId)

        btnTrue.setOnClickListener {
            checkAnswer(true);
        }
        btnFalse.setOnClickListener {
            checkAnswer(false);
        }
        btnNext.setOnClickListener {
            quizViewModel.moveToNext()
            updateQuestion()
        }
        btnBack.setOnClickListener {
            quizViewModel.moveToPrevious()
            updateQuestion()
        }
        updateQuestion()
    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG, "onStart() called")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume() called")
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG, "onPause() called")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG, "onStop() called")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy() called")
    }

    private fun checkAnswer(userAnswer: Boolean){
        val correctAnswer = quizViewModel.currentQuestionAnswer
        val messageResId = if (userAnswer == correctAnswer) {
            R.string.correct_text
        } else {
            R.string.incorrect_text
        }
        Toast.makeText(this, messageResId, Toast.LENGTH_SHORT)
            .show()
    }

    private fun updateQuestion() {
        val questionTextResId = quizViewModel.currentQuestionText
        tvQuestion.setText(questionTextResId)
    }

}