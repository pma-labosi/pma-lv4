package com.example.myapplication

import android.widget.Button

data class Question(val textResId: Int, val answer: Boolean) {

}
